# -*- coding: utf-8 -*-
# pip install GoogleAppEngineCloudStorageClient -t <your_app_directory/lib>
import inspect
import json
import logging

from flask import render_template
from flask import request, jsonify, Blueprint
from google.appengine.api import app_identity
import cloudstorage as gcs

bp_hello = Blueprint('hello', __name__, url_prefix='/hello')

json_str = """
{   "動物": "ライオン",
    "food": ["Meat", "Veggies", "Honey"],
    "fur": "金色",
    "clothes": null,
    "diet": [{"動物": "ガゼル", "food":"grass", "fur": "金色"}]
}
"""


@bp_hello.route('/', methods=['GET'])
def home():
    return render_template('index.html')


@bp_hello.route('/put', methods=['GET'])
def put():
    filename = 'mydata.json'
    # デフォルトGCSバケット名
    bucket_name = app_identity.get_default_gcs_bucket_name()
    logging.info(bucket_name)
    # 保存パス
    gcspath = '/' + bucket_name + '/data/' + filename
    # ファイル作成
    with gcs.open(gcspath, 'w') as gcs_file:
        gcs_file.write(json_str)

    return 'GCS PUT file: %s' % bucket_name


@bp_hello.route('/get', methods=['GET'])
def get():
    filename = 'mydata.json'
    # デフォルトGCSバケット名
    bucket_name = app_identity.get_default_gcs_bucket_name()
    gcspath = '/' + bucket_name + '/data/' + filename
    with gcs.open(gcspath) as cloudstorage_file:
        lines = cloudstorage_file.read()
    return jsonify(json.loads(lines))
